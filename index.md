---
layout: default
permalink: /
---

## What is F-Droid?

F-Droid is an installable catalogue of FOSS (Free and Open Source Software) applications for the Android platform. The client makes it easy to browse, install, and keep track of updates on your device.

![My helpful screenshot](assets/fdroid-screenshot.png)